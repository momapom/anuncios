package com.Anuncios.Anuncios.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Anuncios.Anuncios.Model.Usuario;
import com.Anuncios.Anuncios.Repository.UsuarioRepository;

@Controller
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;
		
	@RequestMapping(path="/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario cadastraUsuario(@RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(path="/usuario/{email}", method=RequestMethod.GET)
	@ResponseBody
	public Usuario buscaUsuarioByEmail(@PathVariable(value="email") String email) {
		return usuarioRepository.findByEmail(email);
	}
	
}
