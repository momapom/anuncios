package com.Anuncios.Anuncios.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Anuncios.Anuncios.Repository.AnuncioRepository;

@Controller
public class AnuncioController {
	@Autowired
	AnuncioRepository anuncioRepository;

	@RequestMapping("/")
	@ResponseBody
	public String Hello() {
		return "Anuncio";
	}
}
