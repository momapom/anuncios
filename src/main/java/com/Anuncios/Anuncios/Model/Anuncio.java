package com.Anuncios.Anuncios.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Anuncio {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnuncio;
	private int idUsuario;
	private String titulo;
	private String dataExpiracao;
	private String status;
	public int getId() {
		return idAnuncio;
	}
	public void setId(int idAnuncio) {
		this.idAnuncio = idAnuncio;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDataExpiracao() {
		return dataExpiracao;
	}
	public void setDataExpiracao(String dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
