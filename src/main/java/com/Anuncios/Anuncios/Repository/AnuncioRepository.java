package com.Anuncios.Anuncios.Repository;

import org.springframework.data.repository.CrudRepository;

import com.Anuncios.Anuncios.Model.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, Integer>{

}
