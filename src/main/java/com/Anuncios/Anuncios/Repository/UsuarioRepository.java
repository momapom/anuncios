package com.Anuncios.Anuncios.Repository;

import org.springframework.data.repository.CrudRepository;

import com.Anuncios.Anuncios.Model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	Usuario findByEmail(String email);
}
