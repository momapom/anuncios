package com.Anuncios.Anuncios.Repository;

import org.springframework.data.repository.CrudRepository;

import com.Anuncios.Anuncios.Model.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, Integer>{

}
